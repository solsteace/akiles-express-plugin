'use strict';

var logger = require('morgan');
var express = require('express');
var bodyParser = require('body-parser');

function ExpressHttp (injector) {
  injector.factory('Router', function () {
    return express.Router;
  });

  injector.constant('Routes', express.Router());

  injector.factory('HttpServer', function (Config, Routes) {
    var app = express();

    app.use(logger(Config.logger.level || 'info'));

    app.bootstrap = function () {
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
    };

    return app;
  });
}

module.exports = ExpressHttp;
